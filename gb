#!/bin/sh

curl_netid_to_duid() {
    while getopts "t:" o; do
        case "${o}" in
            t) OIT_ACCESS_TOKEN=${OPTARG} ;;
        esac
    done
    shift $(($OPTIND - 1))
    curl -s "https://streamer.oit.duke.edu/ldap/people/netid/{$1}?access_token=$OIT_ACCESS_TOKEN" | jq ".[0]" | jq -r ".duid"
}

setup_duid_db() {
    while getopts "d:" o; do
        case "${o}" in
            d) DUID_DB=${OPTARG} ;;
        esac
    done
    sqlite3 $DUID_DB "CREATE TABLE IF NOT EXISTS duids(netid TEXT PRIMARY KEY,duid TEXT NOT NULL)"
}

log_duid() {
    while getopts "t:d:" o; do
        case "${o}" in
            t) OIT_ACCESS_TOKEN=${OPTARG} ;;
            d) DUID_DB=${OPTARG} ;;
        esac
    done
    shift $(($OPTIND - 1))
    setup_duid_db -d $DUID_DB
    sqlite3 $DUID_DB "SELECT netid FROM duids" | grep -q $1 && return
    sqlite3 $DUID_DB "INSERT INTO duids(netid,duid) VALUES('$1','$(curl_netid_to_duid -t $OIT_ACCESS_TOKEN $1)')"
}

netid_to_duid() {
    while getopts "t:d:" o; do
        case "${o}" in
            t) OIT_ACCESS_TOKEN=${OPTARG} ;;
            d) DUID_DB=${OPTARG} ;;
        esac
    done
    shift $(($OPTIND - 1))
    setup_duid_db -d $DUID_DB
    DUID=$(sqlite3 $DUID_DB "SELECT duid FROM duids WHERE netid IS '$1'")
    [ -z "$DUID" ] && log_duid -t $OIT_ACCESS_TOKEN -d $DUID_DB $1 && netid_to_duid -t $OIT_ACCESS_TOKEN -d $DUID_DB $1 || echo $DUID
}

merge_duids_into() {
    # $1 is database file
    # $2 is the table (must have netid column and empty duid column)
    while getopts "t:d:" o; do
        case "${o}" in
            t) OIT_ACCESS_TOKEN=${OPTARG} ;;
            d) DUID_DB=${OPTARG} ;;
        esac
    done
    shift $(($OPTIND - 1))
    sqlite3 $1 "SELECT netid FROM $2" | sort -u | while read -r netid ; do
        log_duid -t $OIT_ACCESS_TOKEN -d $DUID_DB $netid
    done
    sqlite3 $1 "ATTACH '$DUID_DB' AS duid_db" \
            "DROP TABLE IF EXISTS duids" \
            "CREATE TABLE duids(netid TEXT PRIMARY KEY,duid TEXT NOT NULL)" \
            "INSERT INTO duids SELECT * FROM duid_db.duids" \
            "UPDATE $2 SET duid = duids.duid FROM duids WHERE $2.netid = duids.netid" \
            "DROP TABLE duids"
}

gpaof() {
    case "$1" in
        "A+") echo 4.0 ;;
        "A" ) echo 4.0 ;;
        "A-") echo 3.7 ;;
        "B+") echo 3.3 ;;
        "B" ) echo 3.0 ;;
        "B-") echo 2.7 ;;
        "C+") echo 2.3 ;;
        "C" ) echo 2.0 ;;
        "C-") echo 1.7 ;;
        "D+") echo 1.3 ;;
        "D" ) echo 1.0 ;;
        "D-") echo 0.7 ;;
        "F" ) echo 0.0 ;;
    esac
}

case "$1" in
    "curl_netid_to_duid") shift && curl_netid_to_duid "$@" ;;
    "setup_duid_db") shift && setup_duid_db "$@" ;;
    "log_duid") shift && log_duid "$@" ;;
    "netid_to_duid") shift && netid_to_duid "$@" ;;
    "merge_duids_into") shift && merge_duids_into "$@" ;;
    "gpaof") shift && gpaof "$@" ;;
esac
